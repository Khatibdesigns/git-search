//
//  API.swift
//  Git Search
//
//  Created by Khatib Designs on 29/08/2021.
//

import Foundation
import Alamofire
import SwiftyJSON

class Repo {
    
    /* Search Repos */
    
    func search(name: String, page: String, completion: @escaping (([RepoData]) -> Void), failure: @escaping () -> Void) {
      
        let size = "20"
        let headers = ["Content-Type": "application/json"]
        
        AF.request("\(Constants().BASE_URL)q=\(name)&per_page=\(size)&page=\(page)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: HTTPHeaders(headers))
            .responseJSON { response in
                let status = response.response?.statusCode ?? 0
                if status == 200 {
                    if let result = response.value {
                        let json = JSON(result)
                        var deals = [RepoData]()
                        for x in json["items"] {
                            let data = RepoData()
                            let owner = Owner()
                            
                            data.score = Double("\(json["forks"])") ?? 0.0
                            
                            data.id = Int("\(x.1["id"])") ?? 0
                            data.name = "\(x.1["name"])"
                            data.total_count = Int("\(json["id"])") ?? 0
                            data.open_issues = Int("\(x.1["open_issues"])") ?? 0
                            
                            data.node_id = "\(x.1["node_id"])"
                            data.full_name = "\(x.1["full_name"])"
                            data.is_private = "\(x.1["is_private"])"
                            data.url = "\(x.1["url"])"
                            data.html_url = "\(x.1["html_url"])"
                            data.language = "\(x.1["language"])"
                            data.default_branch = "\(x.1["default_branch"])"
                            data.description = "\(x.1["description"])"
                            data.watchers = "\(x.1["watchers"])"
                            data.forks = "\(x.1["forks"])"
                            
                            owner.id = Int("\(x.1["owner"]["id"])") ?? 0
                            
                            owner.login = "\(x.1["owner"]["login"])"
                            owner.node_id = "\(x.1["owner"]["node_id"])"
                            owner.avatar_url = "\(x.1["owner"]["avatar_url"])"
                            owner.url = "\(x.1["owner"]["url"])"
                            owner.html_url = "\(x.1["owner"]["html_url"])"
                            owner.gists_url = "\(x.1["owner"]["gists_url"])"
                            owner.repos_url = "\(x.1["owner"]["repos_url"])"
                            owner.type = "\(x.1["owner"]["type"])"
                            
                            data.owner = owner
             
                            deals.append(data)
                        }
                        completion(deals)
                    }
                } else {
                    failure()
                }
            }
    }
}
