//
//  RepoData.swift
//  Git Search
//
//  Created by Khatib Designs on 29/08/2021.
//

import Foundation
import UIKit

class RepoData: Codable {
    
    var total_count: Int?
    var id: Int?
    var node_id: String?
    var name: String?
    var description: String?
    var full_name: String?
    var is_private: String?
    var url: String?
    var html_url: String?
    var language: String?
    var forks: String?
    var open_issues: Int?
    var watchers: String?
    var default_branch: String?
    var score: Double?
    var owner: Owner?
}

class Owner: Codable {

    var login: String?
    var id: Int?
    var node_id: String?
    var avatar_url: String?
    var url: String?
    var html_url: String?
    var gists_url: String?
    var repos_url: String?
    var type: String?
}
