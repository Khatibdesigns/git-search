//
//  WebViewController.swift
//  Git Search
//
//  Created by Khatib Designs on 29/08/2021.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    var path: String?
    var user: String?
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var userLabel: UILabel!
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupView()
    }
    
    private func setupView() {
        let url = URL(string: "\(path ?? "")")!
        webView.load(URLRequest(url: url))
        webView.navigationDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        
        self.userLabel.text! = "\(self.user ?? "")"
    }
}
