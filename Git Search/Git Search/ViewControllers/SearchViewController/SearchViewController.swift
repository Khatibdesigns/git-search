//
//  SearchViewController.swift
//  Git Search
//
//  Created by Khatib Designs on 29/08/2021.
//

import UIKit
import NVActivityIndicatorView

class SearchViewController: UIViewController, NVActivityIndicatorViewable {

    private var identifier = "ResultTableViewCell"
    private var page = 1
    private var term = ""
    
    private var repos = [RepoData]() {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.repoTableView.reloadData()
            }
        }
    }
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var repoTableView: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    @IBAction func searchAction(_ sender: Any) {
        if term != searchTextField.text! {
            self.repos.removeAll()
        }
        if searchTextField.text! != "" {
            getData(term: "\(searchTextField.text!.replacingOccurrences(of: " ", with: "+"))", page: "\(page)")
        }
    }
    
// This has been removed due to the API search limit & a search button will be added in it's place
//    @IBAction func search_actionTextField(_ sender: UITextField) {
//        getData(term: "\(sender.text!.replacingOccurrences(of: " ", with: "+"))", page: "\(page)")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView()
    }
    
    private func setupView() {
        self.repoTableView.register(UINib(nibName: "\(identifier)", bundle: nil), forCellReuseIdentifier: "\(identifier)")
        self.repoTableView.delegate = self
        self.repoTableView.dataSource = self
        self.repoTableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
    }
    
    private func getData(term: String, page: String) {
        self.startAnimating()
        Repo().search(name: term, page: page) { data in
            self.term = term
            self.stopAnimating()
            self.repos.append(contentsOf: data)
        } failure: {
            self.stopAnimating()
        }
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.identifier, for: indexPath) as! ResultTableViewCell
        
        cell.configureCell(image: "\(self.repos[indexPath.row].owner?.avatar_url ?? "")",
                           name: "\(self.repos[indexPath.row].owner?.login ?? "")",
                           type: "\(self.repos[indexPath.row].owner?.type ?? "")",
                           language: "\(self.repos[indexPath.row].language ?? "")",
                           repo: "\(self.repos[indexPath.row].name ?? "")",
                           description: "\(self.repos[indexPath.row].description ?? "")",
                           watchers: "\(self.repos[indexPath.row].watchers ?? "")",
                           forks: "\(self.repos[indexPath.row].forks ?? "")",
                           branch: "\(self.repos[indexPath.row].default_branch ?? "")")
        
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 232
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row + 1 == self.repos.count {
            self.page = page + 1
            self.getData(term: "\(searchTextField.text!)", page: "\(page)")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        VC.modalPresentationStyle = .fullScreen
        VC.path = "\(self.repos[indexPath.row].html_url ?? "")"
        VC.user = "\(self.repos[indexPath.row].owner?.login ?? "")"
        self.present(VC, animated: true, completion: nil)
    }
}
