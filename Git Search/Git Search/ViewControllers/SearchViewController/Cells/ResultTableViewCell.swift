//
//  ResultTableViewCell.swift
//  Git Search
//
//  Created by Khatib Designs on 29/08/2021.
//

import UIKit
import SDWebImage

class ResultTableViewCell: UITableViewCell {

    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var owner_profileImageView: UIImageView!
    @IBOutlet weak var owner_nameLabel: UILabel!
    @IBOutlet weak var owner_roleLabel: UILabel!
    
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var repoLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var watchersLabel: UILabel!
    @IBOutlet weak var branchLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupView()
    }

    private func setupView() {
        self.holderView.layer.cornerRadius = 8
        self.holderView.clipsToBounds = true
        
        self.clipsToBounds = false
        
        self.owner_profileImageView.layer.cornerRadius = self.owner_profileImageView.frame.height / 2
        self.owner_profileImageView.contentMode = .scaleAspectFill
        self.owner_profileImageView.clipsToBounds = true
    
    }
    
    func configureCell(image: String, name: String, type: String, language: String, repo: String, description: String, watchers: String, forks: String, branch: String) {
        self.owner_profileImageView.sd_setImage(with: URL(string: image), completed: nil)
        self.owner_nameLabel.text! = name
        self.owner_roleLabel.text! = type
        
        self.languageLabel.text! = "(\(language))"
        self.repoLabel.text! = repo
        self.descriptionLabel.text! = description
        
        self.watchersLabel.text! = watchers
        self.forksLabel.text! = forks
        self.branchLabel.text! = branch
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
}
